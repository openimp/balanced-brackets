const tr = require('perl-transliterate');
const modifyRegexpFlags = require('../src/utils');

class Extraction {
  constructor(str) {
    this.str = str || '';
    this.pos = 0;
    this.ZERO = 0;
    this.ONE = 1;
  }

  extractBracketed(delimiter, prefix) {
    let pre = prefix || /\s*/;
    pre = modifyRegexpFlags(pre, 'y');
    pre.lastIndex = this.pos;
    let textRef = this.str;
    const delimeters = this._getDelimiters(delimiter);
    if (delimeters.error) {
      return delimeters;
    }

    const {lDel, rDel} = delimeters;

    return this._matchBracketed(textRef, pre, lDel, rDel);
  }

  _getDelimiters(delimiter) {
    const OCTALS = {
      BEGIN: 0,
      END: 377,
    };

    let lDel = delimiter || '{([<';

    lDel = tr(
      lDel,
      "[](){}<>\\" + OCTALS.BEGIN + "-\\" + OCTALS.END,
      '[[(({{<<',
      'ds'
    );

    let rDel = tr(lDel, '[({<', '])}>');

    if (rDel === '') {
      return this._fail(
        `Did not find a suitable bracket in delimiter: ${delimiter}`
      );
    }

    lDel = lDel.replace(/[^A-Za-z_0-9]/g, '\\$&|').slice(0, -1);
    rDel = rDel.replace(/[^A-Za-z_0-9]/g, '\\$&|').slice(0, -1);

    return {
      lDel,
      rDel,
    };
  }

  _matchBracketed(str, prefix, leftDelimiter, rightDelimiter) {
    let textRef = str;
    let pre = prefix;
    let lDel = leftDelimiter;
    let rDel = rightDelimiter;
    const nesting = [];
    const STARTPOS = 0;
    let lDelPos;
    let endPos;

    const hasPrefix = this._hasPrefix(textRef, pre);
    if (hasPrefix !== true) {
      return hasPrefix;
    }

    lDelPos = this.pos;

    const lDelMatchDetails = this._openingBracket(textRef, lDel, pre);
    if (lDelMatchDetails.error || 
        lDelMatchDetails.matches === null) {
      return lDelMatchDetails;
    }

    let lDelRegExp = lDelMatchDetails.lDelRegExp;
    nesting.push(lDelMatchDetails.matches[this.ZERO]);

    let textLen = textRef.length;
    let allChars = new RegExp(/(?:[a-zA-Z0-9]+|.)/y);
    let rDelRegExp = new RegExp(rDel, 'y');

    while (this.pos < textLen) {
      let hasSlashedChar = this._hasSlashedOrLiteralCharacter(textRef);

      if (hasSlashedChar) {
        continue;
      }

      lDelRegExp.lastIndex = this.pos;
      rDelRegExp.lastIndex = this.pos;

      let lDelTest = lDelRegExp.exec(textRef);
      let rDelTest = rDelRegExp.exec(textRef);
      if (lDelTest && (lDelRegExp.lastIndex - this.pos) === this.ONE) {
        nesting.push(lDelTest[this.ZERO]);
        this.pos += (lDelRegExp.lastIndex - this.pos);
      } else if (rDelTest) {
        this.pos += (rDelRegExp.lastIndex - this.pos);

        if (nesting.length === this.ZERO) {
          return this._fail(`Unmatched closing bracket: \"${rDelTest[this.ZERO]}\"`);
          break;
        }

        let expected = nesting.pop();

        expected = tr(expected, '({[<', ')}]>');

        if (expected !== rDelTest[this.ZERO]) {
          return this._fail(`Mismatched closing bracket: expected: expected \"${expected}\" but found \"${rDelTest}\"`);
          break;
        }

        if (nesting.length === this.ZERO) {
          break;
        }
      } else {
        allChars.lastIndex = this.pos;
        allChars.test(textRef);
        this.pos += allChars.lastIndex - this.pos;
      }
    }

    if (nesting.length > this.ZERO) {
      return this._fail(`Unmatched opening bracket(s): ${nesting.toString()}`);
    }

    endPos = this.pos;

    let prefixStr = {
      start: STARTPOS,
      end: lDelPos-STARTPOS,
    };

    let matchStr = {
      start: lDelPos,
      end: (endPos-lDelPos-2)+2,
    };

    let remainderStr = {
      start: endPos,
      end: textRef.length - endPos,
    };

    return this._succeed(textRef, prefixStr, matchStr, remainderStr);
  }

  _hasPrefix(textRef, prefix) {
    let hasPrefix = prefix.test(textRef);
    this.pos += (prefix.lastIndex) ? prefix.lastIndex - this.pos : this.ZERO;
    if (hasPrefix === false) {
      return this._fail(`Did not find prefix: \"${prefix}\"`);
    }

    return true;
  }

  _openingBracket(textRef, lDel, pre) {
    let lDelRegExp = new RegExp(lDel, 'y');
    lDelRegExp.lastIndex = this.pos;
    const lDelMatchDetails = textRef.match(lDelRegExp);
    this.pos += (lDelRegExp.lastIndex) ? (lDelRegExp.lastIndex - this.pos) : this.ZERO;

    if (lDelMatchDetails === null) {
      return this._fail(`Did not find opening bracket after prefix: \"${pre}\"`);
    }

    return {
      lDelRegExp,
      matches: lDelMatchDetails,
    };
  }

  _hasSlashedOrLiteralCharacter(textRef) {
    let slashedChar = new RegExp(/[\\\n\r\t\f]./, 'y');
    slashedChar.lastIndex = this.pos;
    let hasSlashedChar = slashedChar.test(textRef);

    this.pos += (slashedChar.lastIndex) ? (slashedChar.lastIndex - this.pos) : this.ZERO;
    return hasSlashedChar
  }

  _succeed(textRef, prefix, match, remainder) {
    return {
      extracted: textRef.substr(match.start, match.end),
      remainder: textRef.substr(remainder.start, remainder.end),
      skipped: textRef.substr(prefix.start, prefix.end),
    };
  }

  _fail (error) {
    return {
      extracted: undefined,
      remainder: this.str,
      skipped: undefined,
      error: error
    };
  }
}

module.exports = Extraction;
