function modifyRegexpFlags(old, mod) {
  let newSrc = old.source;
  mod = mod || '';
  if (old.global && !mod.match(/g/i))
    mod += 'g';
  if (old.ignoreCase && !mod.match(/i/i))
    mod += 'i';
  if (old.multiline && !mod.match(/m/i))
    mod += 'm';
  if (old.unicode && !mod.match(/u/i))
    mod += 'u';
  if (old.sticky && !mod.match(/y/i))
    mod += 'y';

  return new RegExp(newSrc, mod);
}

module.exports = modifyRegexpFlags;
