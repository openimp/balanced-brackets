const ExtractBracketed = require('../index');

describe('extractBracketed', () => {
  describe('using custom delimiters and the standard prefix', () => {
    it('should return the expected object when delimiter is found after the standard prefix', () => {
      const expected = {
        extracted: '(abcd)',
        remainder: ' 123',
        skipped: ''
      };

      const extract = new ExtractBracketed('(abcd) 123');
      const actual = extract.extractBracketed('()');
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected object when delimiter is found after the standard prefix and delimiter contains characters', () => {
      const expected = {
        extracted: '(abcd)',
        remainder: ' 123',
        skipped: ''
      };

      const extract = new ExtractBracketed('(abcd) 123');
      const actual = extract.extractBracketed('()abc');
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected error object when an unsuitable delimiter is passed through', () => {
      const expected = {
        extracted: undefined,
        remainder: '(abcd) 123',
        skipped: undefined,
        error: 'Did not find a suitable bracket in delimiter: p'
      };

      const extract = new ExtractBracketed('(abcd) 123');
      const actual = extract.extractBracketed('p');
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected error object when opening bracket after the prefix is not found', () => {
      const str = 'happy(abcd) 123';
      const expected = {
        extracted: undefined,
        remainder: str,
        skipped: undefined,
        error: 'Did not find opening bracket after prefix: \"/\\s*/y\"'
      };

      const extract = new ExtractBracketed(str);
      const actual = extract.extractBracketed('()');
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected error object when an unmatched closing bracket', () => {
      const str = '(abcd 123';
      const expected = {
        extracted: undefined,
        remainder: str,
        skipped: undefined,
        error: 'Unmatched opening bracket(s): ('
      };

      const extract = new ExtractBracketed(str);
      const actual = extract.extractBracketed('()');
      expect(actual).to.be.eql(expected);
    });
  });
  describe('using custom delimiters and a custom prefix', () => {
    it('should return the expected object when looping over a string with multiple brackets', () => {
      const expected = ['(abcd)', '(efgh)'];
      let actual = [];
      const extract = new ExtractBracketed('(abcd) 123 (efgh)');
      while (true) {
        let obj = extract.extractBracketed('()', /[a-z0-9\s]*/i);

        if (!obj.extracted) {
          break;
        } else {
          actual.push(obj.extracted);
        }
      }
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected object when delimiter is found after the prefix', () => {
      const expected = {
        extracted: '(abcd)',
        remainder: ' 123',
        skipped: ''
      };

      const extract = new ExtractBracketed('(abcd) 123');
      const actual = extract.extractBracketed('()', /[a-z0-9\s]*/i);
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected object when string contains a slashed character', () => {
      const expected = { 
        extracted: '(\\n      )', 
        remainder: '', 
        skipped: 'Undisputed ' 
      };

      const extract = new ExtractBracketed('Undisputed (\\n      )');
      const actual = extract.extractBracketed('()', /[^\[\]\(\)\{\}]*/i);
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected object when string contains a literal character', () => {
      const expected = { 
        extracted: '(\n      )', 
        remainder: '', 
        skipped: 'Undisputed ' 
      };

      const extract = new ExtractBracketed('Undisputed (\n      )');
      const actual = extract.extractBracketed('()', /[^\[\]\(\)\{\}]*/i);
      expect(actual).to.be.eql(expected);
    });
    it('should return the expected error object when str does not match the prefix', () => {
      const str = 'abc';
      const pre = /([\[\]\(\)\{\}\<\>])\1*/giy;
      const expected = {
        extracted: undefined,
        remainder: str,
        skipped: undefined,
        error: `Did not find prefix: \"${pre}\"`
      };
      const extract = new ExtractBracketed(str);
      const actual = extract.extractBracketed('{}', pre);
      expect(actual).to.be.eql(expected);
    });
  });
});
