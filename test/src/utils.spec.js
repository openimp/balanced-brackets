const modifyRegexpFlags = require('../../src/utils');

describe('modifyRegexpFlags', () => {
  describe('with mod flags', () => {
    describe('without original flags', () => {
      describe('with g flag', () => {
        it('should produce a regex with a g flag', () => {
          const expected = new RegExp(/[a-z]/g);
          const actual = modifyRegexpFlags(/[a-z]/, 'g');
          expect(actual).to.be.eql(expected);
        });
      });
      describe('with i flag', () => {
        it('should produce a regex with an i flag', () => {
          const expected = new RegExp(/[a-z]/i);
          const actual = modifyRegexpFlags(/[a-z]/, 'i');
          expect(actual).to.be.eql(expected);
        });
      });
      describe('with m flag', () => {
        it('should produce a regex with a m flag', () => {
          const expected = new RegExp(/[a-z]/m);
          const actual = modifyRegexpFlags(/[a-z]/, 'm');
          expect(actual).to.be.eql(expected);
        });
      });
      describe('with y flag', () => {
        it('should produce a regex with a y flag', () => {
          const expected = new RegExp(/[a-z]/y);
          const actual = modifyRegexpFlags(/[a-z]/, 'y');
          expect(actual).to.be.eql(expected);
        });
      });
      describe('with u flag', () => {
        it('should produce a regex with a u flag', () => {
          const expected = new RegExp(/[a-z]/u);
          const actual = modifyRegexpFlags(/[a-z]/, 'u');
          expect(actual).to.be.eql(expected);
        });
      });
      describe('with g and i flag', () => {
        it('should produce a regex with a g and i flag', () => {
          const expected = new RegExp(/[a-z]/gi);
          const actual = modifyRegexpFlags(/[a-z]/, 'gi');
          expect(actual).to.be.eql(expected);
        });
      });
    });
    describe('with an original flag', () => {
      describe('with an original g flag', () => {
        describe('with a mod flag of i', () => {
          it('should produce a regex with an appended i flag', () => {
            const expected = new RegExp(/[a-z]/gi);
            const actual = modifyRegexpFlags(/[a-z]/g, 'i');
            expect(actual).to.be.eql(expected);
          });
        });
      });
      describe('with an original i flag', () => {
        describe('with a mod flag of g', () => {
          it('should produce a regex with an appended g flag', () => {
            const expected = new RegExp(/[a-z]/gi);
            const actual = modifyRegexpFlags(/[a-z]/i, 'g');
            expect(actual).to.be.eql(expected);
          });
        });
      });
      describe('with an original m flag', () => {
        describe('with a mod flag of g', () => {
          it('should produce a regex with an appended g flag', () => {
            const expected = new RegExp(/[a-z]/gm);
            const actual = modifyRegexpFlags(/[a-z]/m, 'g');
            expect(actual).to.be.eql(expected);
          });
        });
      });
      describe('with an original y flag', () => {
        describe('with a mod flag of i', () => {
          it('should produce a regex with an appended i flag', () => {
            const expected = new RegExp(/[a-z]/yi);
            const actual = modifyRegexpFlags(/[a-z]/y, 'i');
            expect(actual).to.be.eql(expected);
          });
        });
      });
      describe('with an original u flag', () => {
        describe('with a mod flag of g', () => {
          it('should produce a regex with an appended g flag', () => {
            const expected = new RegExp(/[a-z]/gu);
            const actual = modifyRegexpFlags(/[a-z]/u, 'g');
            expect(actual).to.be.eql(expected);
          });
        });
      });
      describe('with an original g and i flag', () => {
        describe('with a mod flag of u', () => {
          it('should produce a regex with an appended u flag', () => {
            const expected = new RegExp(/[a-z]/giu);
            const actual = modifyRegexpFlags(/[a-z]/gi, 'u');
            expect(actual).to.be.eql(expected);
          });
        });
      });
    });
  });
});
